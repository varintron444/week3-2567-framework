function a() {
    console.log("callback Function")
}
// Nested function
function sayHi(callback, fname, lname) {
    callback();
    function getFullName() {
        return fname + " " + lname + " " + callback()
    }
    return getFullName()
}
const message = sayHi(a,"Mr.Go", "Anfield")
console.log(message)