function* gen() {
    yield "ohm";
    yield "field";
    yield "what";
}

const mygen = gen();
console.log(mygen.next());
console.log(mygen.next().value);
console.log(mygen.next().value);
