function greet() {
    // outer function
    const name = 'Mr.GT'
    // inner function
    function displayName() {
        return "Hi:"+" "+ name
}
    return displayName()
}
const g1 = greet()
console.log(g1)
console.log(g1())